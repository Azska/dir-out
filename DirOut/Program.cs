﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirOut
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Путь для чтения данных");
            string path = Console.ReadLine();

            Console.WriteLine(@"куда сохранять ( например C:\1\out.txt");
            string savePath = Console.ReadLine();

            Console.WriteLine("Стартуем");
            List<string> result = GetFileInfos(path);
            File.WriteAllLines(savePath, result);
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        static List<string> GetFileInfos(string path)
        {
            List<string> result = new List<string>();
            DirectoryInfo dir_info = new DirectoryInfo(path);
            SearchDirectory(dir_info, result);
            return result;
        }

        static void SearchDirectory(DirectoryInfo dir_info, List<string> data)
        {
            try
            {
                data.Add(dir_info.FullName);
                foreach (DirectoryInfo subdir_info in dir_info.GetDirectories())
                {
                    SearchDirectory(subdir_info, data);
                }
            }
            catch
            {
            }
        }
    }
}
